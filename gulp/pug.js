var gulp = require('gulp')
  , pug = require('gulp-pug')
  , watch = require('gulp-watch')
  , debug = require('gulp-debug')
  , rename = require('gulp-rename')
  , replace = require('gulp-replace')
  , livereload = require('gulp-livereload');

var _config = require('./_config');

var pugs = {
  './src/pug/pages/land1.pug' : 'done/land1/index.html',
  './src/pug/pages/land2.pug' : 'done/land2/index.html',
  './src/pug/pages/land3.pug' : 'done/land3/index.html',
  './src/pug/pages/land4.pug' : 'done/land4/index.html',
  './src/pug/pages/land5.pug' : 'done/land5/index.html',
  './src/pug/pages/land6.pug' : 'done/land6/index.html'
};
var pages = [];

function build_pug(src, name) {
  pages.push({src : name});
  return gulp.src(src)
    .pipe(debug({title : 'Build page from : "' + src + '"'}))
    .pipe(pug({
      pretty : true,
      locals : {
        config : _config.pug
      }
    }))
    .pipe(rename(name))
    .pipe(replace('₽', '<span class="icon-rub"></span>'))
    .pipe(gulp.dest('./dest/'))
    .pipe(livereload());
}

function build_index() {
  return gulp.src('./src/pug/index.pug')
    .pipe(pug({
      locals : {
        config : _config.pug,
        pages  : pages
      }
    }))
    .pipe(gulp.dest('./dest/'));
}

function build() {
  var tasks = [];
  for (var src in pugs)
    if (pugs.hasOwnProperty(src))
      tasks.push(build_pug.bind(null, src, pugs[src]));
  return gulp.series(gulp.parallel.apply(gulp, tasks), build_index);
}

gulp.task('build:pug', build());

gulp.task('watch:pug', function () {
  return watch(['./src/pug/**/*.pug'], {verbose : true}, build())
});

module.exports = {
  build : gulp.parallel('build:pug'),
  watch : gulp.parallel('watch:pug')
};