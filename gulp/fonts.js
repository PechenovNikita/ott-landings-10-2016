var gulp = require('gulp')
  , file = require('gulp-file')
  , rename = require('gulp-rename')
  , fontgen = require('gulp-fontgen');

var _config = require('./_config');
var fontsNeue = [
  {
    file  : 'HelveticaNeueCyr-UltraLight_0.otf',
    name  : 'HelveticaNeueCyr-UltraLight_0',
    width : '100'
  },
  {
    file  : 'HelveticaNeueCyr-Thin.otf',
    name  : 'HelveticaNeueCyr-Thin',
    width : '200'
  },
  {
    file  : 'HelveticaNeueCyr-Light.otf',
    name  : 'HelveticaNeueCyr-Light',
    width : '300'
  },
  {
    file  : 'HelveticaNeueCyr-Roman.otf',
    name  : 'HelveticaNeueCyr-Roman',
    width : '400'
  },
  {
    file  : 'HelveticaNeueCyr-Medium.otf',
    name  : 'HelveticaNeueCyr-Medium',
    width : '500'
  },
  {
    file  : 'HelveticaNeueCyr-Bold.otf',
    name  : 'HelveticaNeueCyr-Bold',
    width : '700'
  },
  {
    file  : 'HelveticaNeueCyr-Black.otf',
    name  : 'HelveticaNeueCyr-Black',
    width : '900'
  }
];

var fontsIntro = [
  {
    file  : 'Intro-Black-Alt.otf',
    name  : 'Intro-Black-Alt',
    width : 900
  },
  {
    file  : 'Intro-Regular-Alt.otf',
    name  : 'Intro-Regular-Alt',
    width : 400
  },
  {
    file  : 'Intro-Light.otf',
    name  : 'Intro-Light',
    width : 300
  }
];

var fontsAgora = [
  {
    file  : 'pfagorasanspro-bold.ttf',
    name  : 'pfagorasanspro-reg',
    width : 700
  },
  {
    file  : 'pfagorasanspro-reg.ttf',
    name  : 'pfagorasanspro-reg',
    width : 400
  }
];

gulp.task('build:fontgen:HelveticaNeue', function () {

  var css = '';
  fontsNeue.forEach(function (font) {
    if (!font.skip) {
      var name = font.name;
      gulp.src('./src/fonts/HelveticaNeue/' + font.file)
        .pipe(fontgen({
          dest : './dest/fonts/HelveticaNeue'
        }));
    }

    css += '' +
      '@font-face {' +
      '  font-family: "HelveticaNeueWeb";' +
      '  src: url("' + _config.less.path.fonts + 'HelveticaNeue/' + font.name + '.eot");' +
      '  src: url("' + _config.less.path.fonts + 'HelveticaNeue/' + font.name + '.eot?#iefix") format("embedded-opentype"),' +
      '      url("' + _config.less.path.fonts + 'HelveticaNeue/' + font.name + '.woff2") format("woff2"),' +
      '      url("' + _config.less.path.fonts + 'HelveticaNeue/' + font.name + '.woff") format("woff"),' +
      '      url("' + _config.less.path.fonts + 'HelveticaNeue/' + font.name + '.ttf") format("ttf"),' +
      '      url("' + _config.less.path.fonts + 'HelveticaNeue/' + font.name + '.svg#' + font.name + '") format("svg");' +
      '  font-style: normal;' +
      '  font-weight: ' + font.width + ';' +
      '}';
  });

  return file('fonts-neue.css', css, {src : true})
    .pipe(gulp.dest('./dest/css'));

});

gulp.task('build:fontgen:Intro', function () {

  var css = '';
  fontsIntro.forEach(function (font) {
    if (!font.skip) {
      var name = font.name;
      gulp.src('./src/fonts/Intro/' + font.file)
        .pipe(fontgen({
          dest : './dest/fonts/Intro'
        }));
    }

    css += '' +
      '@font-face {' +
      '  font-family: "IntroWeb";' +
      '  src: url("' + _config.less.path.fonts + 'Intro/' + font.name + '.eot");' +
      '  src: url("' + _config.less.path.fonts + 'Intro/' + font.name + '.eot?#iefix") format("embedded-opentype"),' +
      '      url("' + _config.less.path.fonts + 'Intro/' + font.name + '.woff2") format("woff2"),' +
      '      url("' + _config.less.path.fonts + 'Intro/' + font.name + '.woff") format("woff"),' +
      '      url("' + _config.less.path.fonts + 'Intro/' + font.name + '.ttf") format("ttf"),' +
      '      url("' + _config.less.path.fonts + 'Intro/' + font.name + '.svg#' + font.name + '") format("svg");' +
      '  font-style: normal;' +
      '  font-weight: ' + font.width + ';' +
      '}';
  });

  return file('fonts-intro.css', css, {src : true})
    .pipe(gulp.dest('./dest/css'));

});

gulp.task('build:fontgen:Agora', function () {

  var css = '';
  fontsAgora.forEach(function (font) {
    if (!font.skip) {
      var name = font.name;
      gulp.src('./src/fonts/Agora/' + font.file)
        .pipe(fontgen({
          dest : './dest/fonts/Agora'
        }));
    }

    css += '' +
      '@font-face {' +
      '  font-family: "PFAgoraWeb";' +
      '  src: url("' + _config.less.path.fonts + 'Agora/' + font.name + '.eot");' +
      '  src: url("' + _config.less.path.fonts + 'Agora/' + font.name + '.eot?#iefix") format("embedded-opentype"),' +
      '      url("' + _config.less.path.fonts + 'Agora/' + font.name + '.woff2") format("woff2"),' +
      '      url("' + _config.less.path.fonts + 'Agora/' + font.name + '.woff") format("woff"),' +
      '      url("' + _config.less.path.fonts + 'Agora/' + font.name + '.ttf") format("ttf"),' +
      '      url("' + _config.less.path.fonts + 'Agora/' + font.name + '.svg#' + font.name + '") format("svg");' +
      '  font-style: normal;' +
      '  font-weight: ' + font.width + ';' +
      '}';
  });

  return file('fonts-agora.css', css, {src : true})
    .pipe(gulp.dest('./dest/css'));

});

module.exports = {
  build : gulp.parallel('build:fontgen:HelveticaNeue', 'build:fontgen:Intro', 'build:fontgen:Agora')
};