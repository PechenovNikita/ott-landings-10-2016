var gulp = require('gulp')
  , less_data = require('gulp-less-data')
  , less = require('gulp-less')
  , clean_css = require('gulp-clean-css')
  , rename = require('gulp-rename')
  , debug = require('gulp-debug')
  , livereload = require('gulp-livereload')
  , watch = require('gulp-watch');

var _config = require('./_config');

var lessData = {
  pathImg : '"' + _config.less.path.images + '"',
  pathFonts : '"' + _config.less.path.fonts + '"'
};
var lessFiles = {
  './src/less/layout.less'      : 'layout.css',
  './src/less/components.less'  : 'components.css',
  './src/less/pages/land1.less' : 'land1.css',
  './src/less/pages/land2.less' : 'land2.css',
  './src/less/pages/land3.less' : 'land3.css',
  './src/less/pages/land4.less' : 'land4.css',
  './src/less/pages/land5.less' : 'land5.css',
  './src/less/pages/land6.less' : 'land6.css'
};

function build_less_file(src, name) {
  return gulp.src(src)
    .pipe(debug({title : 'Find less file :'}))
    .pipe(less_data(lessData))
    .pipe(less())
    .pipe(rename(name))
    // .pipe(gulp.dest('./dest/css'))
    // .pipe(debug({title : 'Compile less file :'}))
    // .pipe(livereload())
    .pipe(clean_css())
    .pipe(rename(function (file) {
      file.basename += '.min';
    }))
    .pipe(gulp.dest('./dest/css'))
    .pipe(debug({title : 'Minify css file :'}))
    .pipe(livereload());
}

function build_less() {
  var tasks = [];
  for (var src in lessFiles) {
    if (lessFiles.hasOwnProperty(src)) {
      tasks.push((build_less_file).bind(null, src, lessFiles[src]))
    }
  }
  return gulp.parallel.apply(gulp, tasks);
}

gulp.task('build:less', build_less());
gulp.task('watch:less', function () {
  return watch(['./src/less/**/*.less'], {verbose : true}, build_less())
});

module.exports = {
  build : gulp.parallel('build:less'),
  watch : gulp.parallel('watch:less')
};