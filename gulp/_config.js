var fs = require('fs');
var absolutePath = 'http://landing-name.ru/';

if (fs.existsSync('./gulp/local/config.js')) {
  // absolutePath = require('./local/config').path;
}

console.log(absolutePath);

module.exports = {
  less : {
    path : {
      images : absolutePath + 'images/',
      fonts  : absolutePath + 'fonts/'
    }
  },
  pug  : {
    path : {
      absolute : absolutePath,
      css      : absolutePath + 'css/',
      images   : absolutePath + 'images/',
      js       : absolutePath + 'js/'
    }
  }
};