var gulp = require('gulp')
  , image = require('gulp-image')
  , image_min = require('gulp-imagemin')
  , resize = require('gulp-image-resize')
  , clean = require('gulp-clean')
  , gulpif = require('gulp-if')
  , debug = require('gulp-debug')
  , watch = require('gulp-watch');

var livereload = require('gulp-livereload')

var fs = require('fs');

var imageMinConfig = {verbose : true};
var imageConfig = {
  pngquant       : true,
  optipng        : false,
  zopflipng      : false,
  jpegRecompress : false,
  jpegoptim      : true,
  mozjpeg        : true,
  gifsicle       : true,
  svgo           : false,
  concurrent     : 10
};

function getFileOptions(vynil) {
  var file = vynil.path.replace(vynil.cwd, '.');
  var path = file.replace('./src/images/', '').split('/');

  if (path.length > 1)
    path = path[0];
  else
    path = '';

  var name = (vynil.path.replace(vynil.cwd, '.')).replace('./src/images/', '');
  var ext = name.split('.');
  ext = ext[ext.length - 1];

  return {
    src  : vynil.path.replace(vynil.cwd, '.'),
    path : path,
    name : name,
    ext  : ext
  };
}

function clean_image(done) {
  if (fs.existsSync('./dest/images')) {
    return gulp.src('./dest/images')
      .pipe(clean());
  }
  done();
}

function build_all_image_nosvg() {
  return gulp.src(['./src/images/**/*.png', './src/images/**/*.jpg', './src/images/**/*.jpeg'])
    .pipe(image(imageConfig))
    .pipe(gulp.dest('./dest/images'));
}
function build_all_image_svg() {
  return gulp.src('./src/images/**/*.svg')
    .pipe(image_min(imageMinConfig))
    .pipe(gulp.dest('./dest/images'));
}

gulp.task('watch:image', function () {
  return watch(['./src/images/**/*.{svg|png|jpg|jpeg}'], {
    verbose : true
  }, function (vynil) {
    var options = getFileOptions(vynil);
    var isSvg = false, isPng = false;

    switch (options.ext) {
      case 'jpg':
      case 'jpeg':
      case 'png':
        isPng = true;
        break;
      case 'svg':
        isSvg = true;
        break;
    }

    var path = (options.path == '' ? '' : options.path + '/');
    if (fs.existsSync(options.src) && (isPng || isSvg)) {
      return gulp.src(options.src)
        .pipe(gulpif(isPng, image(imageConfig)))
        .pipe(gulpif(isSvg, image_min(imageMinConfig)))
        .pipe(gulp.dest('./dest/images/' + path));
    } else {
      fs.exists('./dest/images/' + path + options.name, function (exists) {
        if (exists) {
          console.log('Delete file ' + './dest/images/' + path + options.name);
          fs.unlink('./dest/images/' + path + options.name);
        }
      });
    }
  });
});

gulp.task('build:image',
  gulp.series(
    clean_image
    , gulp.parallel(
      build_all_image_nosvg
      , build_all_image_svg
    )
  )
);

module.exports = {
  build : gulp.parallel('build:image'),
  watch : gulp.parallel('watch:image')
};