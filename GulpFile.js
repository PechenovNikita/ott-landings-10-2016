var gulp = require('gulp');
var livereload = require('gulp-livereload');

var images = require('./gulp/images')
  , js = require('./gulp/js')
  , pug = require('./gulp/pug')
  , fonts = require('./gulp/fonts')
  , less = require('./gulp/less');

gulp.task('build', gulp.series(fonts.build, gulp.parallel(less.build, pug.build)));

gulp.task('watch', gulp.series(function live(done) {
  livereload.listen();
  done();
}, gulp.parallel(images.watch, less.watch, pug.watch)));

